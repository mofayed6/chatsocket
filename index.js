let express = require('express');
let socket = require('socket.io')

let app = express();
let server = app.listen(4000,function () {
    console.log('listen server port 4000');
});

app.use(express.static('public'));

var io = socket(server);

io.on('connection',function (socket) {
    socket.on('chat',function (data) {
        io.sockets.emit('chat',data)
    });

    socket.on('typing',function (data) {
        socket.broadcast.emit('typing',data)
    });
});